<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <documentation>
        <title>Magnetic Network defining a simple transformator with resistive load</title>
        <authors>
            <author>ftoth</author>
        </authors>
        <date>2018-09-19</date>
        <keywords>
            <keyword>magneticEdge</keyword>
        </keywords>
        <references> </references>
        <isVerified>no</isVerified>
        <description> 
Simple magnetic network of a conducting wire though a magnetic core.
The BCs are set such that magnetic flux is possible only in x-direction.
The wire is excited by current in z-direction.

We selcect the geometry such that we have unit-areas and lengths.

For a comparison with the analysic solution see the iPython notebook.
        </description>
    </documentation>
    <fileFormats>
        <input>
            <!--<hdf5 fileName="NetworkHeat_HarmonicIterative.h5ref"/>-->
            <gmsh fileName="SimpleTrafo.msh"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>
    <domain geometryType="3d" printGridInfo="yes">
        <variableList>
            <var name="a" value="1.0"/>
            <var name="b1" value="2.0"/>
            <var name="b2" value="1.0"/>
            <var name="b3" value="b2"/>
            <var name="b4" value="b1"/>
            <var name="c" value="1.0"/>
            <var name="I_coil" value="ICOIL"/>
            <var name="f_coil" value="FCOIL"/>
            <var name="N_periods" value="5"/>
            <var name="N_per_period" value="80"/>
        </variableList>
        <regionList>
            <region name="V1" material="MAT"/>
            <region name="V2" material="Resistor"/>
            <region name="V3" material="Coil"/>
            <region name="V4" material="MAT"/>
        </regionList>
        <surfRegionList> </surfRegionList>
    </domain>
    
    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>0</isoOrder>
        </Legendre>
    </fePolynomialList>
    
    <sequenceStep index="1">
        <analysis>
            <!--<static/>-->
            <transient>
                <numSteps>N_periods*N_per_period</numSteps>
                <deltaT>1/f_coil/N_per_period</deltaT>
            </transient>
        </analysis>
        
        <pdeList>
            <magneticEdge>
                <regionList>
                    <region name="V1" polyId="Hcurl" nonLinIds="perm"/>
                    <region name="V2" polyId="Hcurl"/>
                    <region name="V3" polyId="Hcurl"/>
                    <region name="V4" polyId="Hcurl" nonLinIds="perm"/>
                </regionList>
                
                <nonLinList>
                    <permeability id="perm"/>
                </nonLinList>
                
                <bcsAndLoads>
                    <fluxParallel name="S_T"/>
                    <fluxParallel name="S_B"/>
                    <fluxParallel name="S_N"/>
                    <fluxParallel name="S_S"/>
                </bcsAndLoads>
                
                <coilList>
                    <coil id="coil">
                        <source type="current" value="I_coil*fadeIn((N_periods-2)/f_coil,1,t)*cos(2*pi*f_coil*t)"/><!-- *sqrt(1-pi*pi*f*f)/(1+pi*pi*f*f)  -->
                        <part id="1">
                            <regionList>
                                <region name="V3"/>
                            </regionList>
                            <direction>
                                <analytic>
                                    <comp dof="z" value="1"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="b3*a"/>
                        </part>
                    </coil>
                </coilList>
                
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magCoilCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotentialD1">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magJouleLossPowerDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magFieldIntensity">
                        <allRegions/>
                    </elemResult>   
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>  
                    <surfRegionResult type="magFlux">
                        <surfRegionList>
                            <surfRegion name="S1_W" outputIds="txt"/>
                        </surfRegionList>
                    </surfRegionResult>
                    <coilResult type="coilInducedVoltage">
                        <coilList>
                            <coil id="coil" outputIds="txt"/>
                        </coilList>
                    </coilResult>
                </storeResults>
            </magneticEdge>
        </pdeList>
        
        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <nonLinear logging="yes" method="newton">
                            <lineSearch/>
                            <incStopCrit> 1e-3</incStopCrit>
                            <resStopCrit> 1e-3</resStopCrit>
                            <maxNumIters> 20  </maxNumIters>
                        </nonLinear>
                    </standard>
                </solutionStrategy>
            </system>
        </linearSystems>
    </sequenceStep>
</cfsSimulation>
