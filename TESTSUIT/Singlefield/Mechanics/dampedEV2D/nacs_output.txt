=======================================================================
=======================================================================
|                                                                     |
|                                 N A C S                             |
|                  Numerical Analysis of Coupled Systems              |
|                                                                     |
|                                                                     |
|  Version:  1.6.SR1.1704                                             |
|                                                                     |
| (c) SIMetris GmbH 2007 - 2012                                       |
|                                                                     |
=======================================================================
=======================================================================



   *****************************
       LICENSE INFORMATION   

    Licensee: SIMetris GmbH, Erlangen
    Expiry Date: 2099-12-31
   *****************************


   NACS run started at 2012-May-07 17:29:26 on sim35

++ Generating object for handling file-IO .......................    OK
++ Reading parameters from file '/home/local/ahauck/NACS/versions/trunk/src/testsuite/singlefield/mechanics/dampedEV2D/dampedEV2D.xml'     OK
++ Generating mesh reader .......................................    OK
++ Generating material reader ...................................    OK
++ Generating remaining output files ............................    OK
++ Reading in the mesh ..........................................    OK
++ Generating driver ............................................    OK
++ Creating PDE 'mechanic' ......................................    OK
++ Starting to solve problem .................................... 
++ Running quadratic eigenvalue problem with the following settings
       number of frequencies       :  12
       type of eigenvalues (which) :  LM
       applied shift               :  0
       convergence tolerance       :  1e-10
       maximum number of iterations:  13000
       number of Arnoldi vectors   :  24



Frequency [Hz]       | Damping              | Errorbound          
------------------------------------------------------------
254.661              | -5.0128              | 1.69135e-09         
636.802              | -5.08005             | 1.87777e-09         
681.932              | -5.09179             | 1.80949e-09         
1133.78              | -5.25374             | 1.78037e-09         
1144.01              | -5.25834             | 6.28749e-09         
1378.2               | -5.37493             | 4.27128e-09         

++ Finished solving the problem .................................    OK


======================================================================
TOTAL TIME:              Wall clock: 1 s         CPU: 0.38 s
======================================================================
