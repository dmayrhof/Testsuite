<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd"
 xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>ElecCondTensorFromFile</title>
    <authors>
      <author>Manfred Kaltenbacher</author>
      <author>Vahid Badeli</author>
    </authors>
    <date>2023-03-28</date>
    <keywords>
      <keyword>elecQuasistatic</keyword>
    </keywords>
    <references> 
      -
    </references>
    <isVerified>no</isVerified>
    <description>
      Functionality test for reading in a conductivity tensor field  from a prior (Computational Fluid Dynamics (CFD)) computation. 
      The 3D test consists of a section of an aorta inside the thorax. 
      We prescribe the normal current density via a constraint on one side and set the other side to 0V (ground). 
      The conductivity is given as a tensor field from a previous computation (input via .h5 file). 
      This is a functionality test that shall test the usage of the tensorial conductivity from a previous computation. 
      The patterns in the results stem from the very coarse discretization (1 element for the thorax) and have been verified against a finer model.
    </description>
  </documentation>
    
  <fileFormats>
    <input>
      <hdf5 fileName="ElectCond_FEM.h5"/>
    </input>
    <output>
      <hdf5 id="hdf5"/>
      <text id="txt"/>
    </output>
    <materialData file="matTensor.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d">
    <variableList>
      <var name="I" value="5.0"/>
    </variableList>
    <regionList>
      <region name="aorta" material="matAorta"/>  
      <region name="thorax" material="matThorax"/>  
    </regionList>
    
    <surfRegionList>
      <surfRegion name="S_inj_load"/>
      <surfRegion name="S_inj_ground"/>
    </surfRegionList>
    
    <nodeList>
    	<nodes name="P_1">
    		<coord x="0.225" y="0.037" z="-0.015"/>
    	</nodes>
    	<nodes name="P_2">
    		<coord x="0.275" y="0.039686" z="-0.015"/>
        </nodes>
    </nodeList>
  </domain>
   
  <sequenceStep>
    <analysis>
      <harmonic>
        <numFreq>1</numFreq>
        <startFreq>1e5</startFreq>
        <stopFreq>1e5</stopFreq>
      </harmonic>
    </analysis>
    
    <pdeList>
      <elecQuasistatic>
        <regionList>
          <region name="aorta" matDependIds="cond"/>
          <region name="thorax"/>
        </regionList>
     
        <matDependencyList>
          <elecConductivity id="cond">
            <grid> 
              <defaultGrid quantity="ElectCond"  dependtype="CONST" snapToCFSTimeStep="true" sequenceStep="1"/>
            </grid>
          </elecConductivity>
        </matDependencyList>
      
        <bcsAndLoads>
          <ground name="S_inj_ground"/>
          <constraint name="S_inj_load"/> 
          <normalCurrentDensity name="S_inj_load" value="I"/>
        </bcsAndLoads>
     
        <storeResults>
          <nodeResult type="elecPotential">
            <allRegions/>
            <nodeList>
              <nodes name="P_1" outputIds="txt,hdf5"/>
              <nodes name="P_2" outputIds="txt,hdf5"/>
            </nodeList>
          </nodeResult>          
          <elemResult type="elecFieldIntensity">
            <allRegions/>
          </elemResult>
          <elemResult type="elecCurrentDensity">
            <allRegions/>
          </elemResult>   
          <elemResult type="displacementCurrentDensity">
            <allRegions />
          </elemResult>     
          <elemResult type="electricAndDisplacemetCurrentDensity">
            <allRegions />
          </elemResult>
          <surfRegionResult type="elecCurrent">
            <surfRegionList>
              <surfRegion name="S_inj_load" outputIds="txt,hdf5" />
            </surfRegionList>
          </surfRegionResult>
          <surfRegionResult type="displacementCurrent">
            <surfRegionList>
              <surfRegion name="S_inj_load" outputIds="txt,hdf5" />
            </surfRegionList>
          </surfRegionResult>
          <surfRegionResult type="elecAndDisplacementCurrent">
            <surfRegionList>
              <surfRegion name="S_inj_load" outputIds="txt,hdf5" />
            </surfRegionList>
          </surfRegionResult>    
        </storeResults>
      </elecQuasistatic>
    </pdeList>
  </sequenceStep>
</cfsSimulation>