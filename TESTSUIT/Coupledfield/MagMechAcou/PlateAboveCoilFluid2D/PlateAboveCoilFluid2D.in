! ==================================================
!  2D plane magnetoc-mechanic-acoustic example
! ==================================================
!
fini
/uis,msgpop,3        ! disable warning popups
/clear

! definition of output filename ===================
/filname,PlateAboveCoilFluid2D,1

/prep7
!/pnum,kp,1
!/pnum,line,1
!/pnum,area,1
!!shpp,off
/uis,msgpop,2        ! enable warning popups

! initialize macros for .mesh-interface ===========
init

! ====================================
!  Parameter Definition
! ====================================

! overall width
w = 40e-3

! coil parameters
coil_dist  =  10e-3
coil_h     = 20e-3
coil_w     =  5e-3

! plate thickness
plate_h    =  4e-3
airgap_h   =  2e-3

! fluid height
fluid_h      = 50e-3

! general model epsilon
eps=1e-9

! gemeral mesh size
h = 2e-3

! acoustic mesh size
h_acou = h

! ====================================
!  Geometry Creation
! ====================================
rectng,0,coil_dist,0,coil_h
rectng,coil_dist,coil_dist+coil_w,0,coil_h
rectng,coil_dist+coil_w,w,0,coil_h

actY = coil_h
rectng,0,coil_dist,actY,actY+airgap_h
rectng,coil_dist,coil_dist+coil_w,actY,actY+airgap_h
rectng,coil_dist+coil_w,w,actY,actY+airgap_h

actY = actY + airgap_h
rectng,0,coil_dist,actY,actY+plate_h
rectng,coil_dist,coil_dist+coil_w,actY,actY+plate_h
rectng,coil_dist+coil_w,w,actY,actY+plate_h

actY = actY + plate_h
rectng,0,coil_dist,actY,actY+fluid_h
rectng,coil_dist,coil_dist+coil_w,actY,actY+fluid_h
rectng,coil_dist+coil_w,w,actY,actY+fluid_h

height = actY+fluid_h
! glue everything togehter
allsel
nummrg,kp

! ====================================
!  Component Creation
! ====================================

! coil
asel,s,loc,x,coil_dist,coil_dist+coil_w
asel,r,loc,y,0,coil_h
cm,coil_a,area

! plate
asel,s,loc,x,0,w
asel,r,loc,y,coil_h+airgap_h,coil_h+airgap_h+plate_h
cm,plate_a,area

! air
asel,s,loc,x,0,w
asel,r,loc,y,0,coil_h+airgap_h
cmsel,u,coil_a
cm,air_a,area

! fluid
asel,s,loc,x,0,w
asel,r,loc,y,coil_h+airgap_h+plate_h,coil_h+airgap_h+plate_h+fluid_h
cm,fluid_a,area

! absorbing boundary condition
lsel,s,loc,x,0,w
lsel,r,loc,y,height
cm,tmp,line

lsel,s,loc,x,w
lsel,r,loc,y,height-fluid_h,height
cmsel,a,tmp
cm,abc_l,line
cmdele,tmp

! fixation of plate
lsel,s,loc,x,w
lsel,r,loc,y,coil_h+airgap_h,coil_h+airgap_h+plate_h
cm,fix_l,line

! symmetry of plate
lsel,s,loc,x,0
lsel,r,loc,y,coil_h+airgap_h,coil_h+airgap_h+plate_h
cm,sym_l,line

! coupling interface
lsel,s,loc,x,0,w
lsel,r,loc,y,height-fluid_h
cm,coupling_l,line

! magnetic boundary
allsel
lsel,s,ext
lsel,u,loc,y,0
cm,pot_l,line

! ====================================
!  Generate Mesh
! ====================================
setelems,'quadr'

! Mesh non-fluid part
esize,h
cmsel,s,air_a
cmsel,a,coil_a
cmsel,a,plate_a
amesh,all

! Mesh fluid part
esize,h_acou
cmsel,s,fluid_a
amesh,all

! Mesh boundary part
setelems,'2d-line'
cmsel,s,abc_l
cmsel,a,fix_l
cmsel,a,sym_l
cmsel,a,coupling_l
cmsel,a,pot_l
lmesh,all

! ====================================
!  Write output
! ====================================
! Compress all numbers
allsel
numcmp,node
numcmp,elem

! write all nodes
allsel
wnodes

! write regions
cmsel,s,air_a
esla
welems,'air'

cmsel,s,coil_a
esla
welems,'coil'

cmsel,s,plate_a
esla
welems,'plate'

cmsel,s,fluid_a
esla
welems,'fluid'

! write all surface element within one region
cmsel,s,abc_l
cmsel,a,fix_l
cmsel,a,sym_l
cmsel,a,coupling_l
cmsel,a,pot_l
esll
welems,'_surf_'

! write single surface "regions" as named elements
cmsel,s,abc_l
esll
wsavelem,'abc'

cmsel,s,fix_l
esll
wsavelem,'fix'

cmsel,s,sym_l
esll
wsavelem,'sym'

cmsel,s,coupling_l
esll
wsavelem,'coupling'

cmsel,s,pot_l
esll
wsavelem,'pot'

! generate mesh
mkmesh
