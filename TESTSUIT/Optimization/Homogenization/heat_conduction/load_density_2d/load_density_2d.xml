<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>Heat_conduction_load_density_2d</title>
    <authors>
      <author>Bich Ngoc Vu</author>
    </authors>
    <date>2020-04-20</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>Andreassen et al.: How to determine composite material properties using numerical homogenization </references>
    <isVerified>yes</isVerified>
    <description> Demonstrate the calculation of thermal conductivity by asymptotic homogenization of the static
    heat equation. Instead of defining two materials (void and solid) with two regions, we make use of pseudo-density scaling. 
    </description>
  </documentation>  
  
  <fileFormats>
    <output>
      <hdf5 directory="./" />
      <info/>
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="mech" material="99lines"/>
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <heatConduction>
        <regionList>
          <region name="mech"/>
        </regionList>

        <bcsAndLoads>
           <periodic secondary="west" primary="east" quantity="heatTemperature"/>
           <periodic secondary="south"  primary="north" quantity="heatTemperature" />
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions/>
          </nodeResult>
          <elemResult type="physicalPseudoDensity">
            <allRegions/>
          </elemResult>
       </storeResults>
      </heatConduction>
    </pdeList>
    <linearSystems>
      <system>
        <solverList>
          <directLDL/> 
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
  
  <loadErsatzMaterial file="load_density_2d.density.xml"/>

  <optimization log="">
    <costFunction type="homTensor" task="minimize" multiple_excitation="true">
      <multipleExcitation type="homogenizationTestStrains" sequence="1"/>
    </costFunction>
    <optimizer type="evaluate" maxIterations="1"/>
    <ersatzMaterial method="simp" material="heat">
      <regions>
        <region name="mech"/>
      </regions>
      <design name="density" initial="1" physical_lower="1e-9" upper="1" region="mech"/>
      <transferFunction type="identity" application="heat" design="density"/>
    </ersatzMaterial>
    <commit mode="each_forward"/>
  </optimization>  
</cfsSimulation>
